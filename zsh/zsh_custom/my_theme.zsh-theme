# -*- mode: shell-script; -*-

## utils
function prompt_char {
    printf '○'
}

## actual functions
function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || printf ${SHORT_HOST:-$HOST}
}

function get_user {
    printf $USER
}

function current_dir {
    printf '${PWD/#$HOME/~}'
}

function precmd() {
    # print a newline before the prompt, unless it's the
    # first prompt in the process.
    if [ -z "$NEW_LINE_BEFORE_PROMPT" ]; then
        NEW_LINE_BEFORE_PROMPT=1
    elif [ "$NEW_LINE_BEFORE_PROMPT" -eq 1 ]; then
        printf "\n"
    fi
}

function prompt() {
    # first banner
    printf '%%{$FG[239]%%}╭─%%{$reset_color%%} '

    for piece in get_user,"at","034" box_name,"in","033" current_dir,"","184"
    do
        IFS=',' read func after color <<< "${piece}"

        # retrieve chunk and save current length
        local chunk=$($func)

        # print string after that, add space if non-empty
        printf '%%{$FG[%b]%%}%b%%{$reset_color%%}' $color $chunk
        if [[ ! -z  $chunk ]]; then
            printf " "
        fi

        # print string after that, add space if non-empty
        printf '%%{$FG[%b]%%}%b%%{$reset_color%%}' 239 $after
        if [[ ! -z $after ]]; then
            printf " "
        fi
    done

    printf "\b"
    printf ' ${$(virtualenv_prompt_info)}'

    # final prompt
    printf '\n%%{$FG[239]%%}╰─$(prompt_char)%%{$reset_color%%} '

}

PROMPT="$(prompt)"

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$FG[239]%}on%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$FG[202]%} ✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$FG[040]%} ✔"
