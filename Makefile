# create backup of file or folder
define create_backup
	test -f $(1) && mv $(1) $(1).bak || /bin/true
endef

# create a symlink, creating folder and backup if needed
define simple_link
	mkdir -p $(dir $(2))
	$(call create_backup,$(2))

	ln -s $(realpath $(1)) $(2)
endef

# group targets
install: emacs-conf tmux-conf zsh-conf
update: zsh/zshrc emacs/init.el

pc-conf: git-conf kde-conf xmodmap-conf
kde-conf: baloo-conf audacious-conf

# emacs
emacs-conf: emacs/init.el
	$(call create_backup,~/.emacs)
	$(call create_backup,~/.emacs.d)

	ln -s $(realpath emacs/)/ ~/.emacs.d

emacs/init.el:
	emacs --batch --visit emacs/init.org -f org-babel-tangle

# tmux
tmux-conf:
	$(call simple_link,tmux/tmux.conf,~/.tmux.conf)

# zsh
zsh-conf: zsh/zshrc
	$(call simple_link,zsh/zshrc,~/.zshrc)

zsh/zshrc:
	yes "yes" | emacs --batch --visit zsh/zshrc.org -f org-babel-tangle

# personal computer
audacious-conf:
	$(call simple_link, \
		kde/audacious-enqueue.desktop, \
		~/.local/share/kservices5/ServiceMenus/audacious-enqueue.desktop)

baloo-conf:
	$(call simple_link,kde/baloo.conf,~/.config/baloo/config)

git-conf:
	$(call create_backup,~/.gitconfig)
	$(call simple_link,git/git.conf,~/.config/git/config)

xmodmap-conf:
	$(call simple_link,xmodmap/xmodmap.conf,~/.config/xmodmap/config)
	$(call simple_link,xmodmap/xmodmap.desktop,~/.config/autostart/xmodmap.desktop)
